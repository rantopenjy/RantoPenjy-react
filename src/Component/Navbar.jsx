import '../App.css'
import {useEffect} from "react";

function Navbar()
{
    return(
        <>
            <nav className="navbar navbar-dark bg-dark shadow">
                <div className="container">
                    <div className="" id="navbarLinks1">
                        <ul className="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" aria-current="page" href="#">About</a>
                            </li>
                        </ul>
                    </div>

                    <a className="navbar-brand" href="#">
                        <h2 className="brand-logo animate__animated animate__flipInX">RantoPenjy</h2>
                    </a>

                    <div className="" id="navbarLinks2">
                        <ul className="navbar-nav me-auto my-2 my-lg-0 navbar-nav-scroll">
                            <li className="nav-item">
                                <a className="nav-link active" aria-current="page" href="#">Home</a>
                            </li>
                            <li className="nav-item">
                                <a className="nav-link" aria-current="page" href="#">About</a>
                            </li>
                        </ul>
                    </div>

                </div>
            </nav>
        </>
    )
}

export default Navbar