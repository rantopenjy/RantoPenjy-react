import '../App.css'
import {useEffect, useState} from "react";

function LoadingPage() {
    // Change the color theme of the website depending on the user's preference using hooks of react
    const [colorTheme, setColorTheme] = useState(window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light');

    useEffect(() => {
        const handleColorTheme = () => {
            setColorTheme(window.matchMedia('(prefers-color-scheme: dark)').matches ? 'dark' : 'light');
        };

        window.matchMedia('(prefers-color-scheme: dark)').addEventListener('change', handleColorTheme);

        return () => {
            window.matchMedia('(prefers-color-scheme: dark)').removeEventListener('change', handleColorTheme);
        };
    }, []);

    useEffect(() => {
        if (colorTheme === 'dark') {
            document.body.style.backgroundColor = '#121212';
        } else {
            document.body.style.backgroundColor = '#fff';
        }
    }, [colorTheme]);

    const handleLogo = () => {
        if (colorTheme === 'dark') {
            return (
                <h1 className="brand-logo text-white text-center">RantoPenjy</h1>
            );
        } else {
            return (
                <h1 className="brand-logo text-black text-center">RantoPenjy</h1>
            );
        }
    };

    return (
        <div className="loading-logo">
            {handleLogo()}
        </div>
    );
}

export default LoadingPage