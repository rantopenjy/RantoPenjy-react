import './App.css'
import {useEffect, useState} from "react";
import { Animated } from 'react-animated-css'
import Navbar from "./Component/Navbar.jsx";
import LoadingPage from "./Component/LoadingPage.jsx";

function App()
{
    const [loading, setLoading] = useState(true)

    useEffect(() => {
        const timer = setTimeout(() => {
            setLoading(false)
        }, 8000)

        return () => clearTimeout(timer)
    }, [])

    if (loading)
    {
        return (
            <LoadingPage></LoadingPage>
        )
    }

    return (
        <>
        {/* animated navbar when showed with Animated from react-animated-css */}
            <Animated animationIn="fadeInDown" animationOut="fadeOut" isVisible={true} animateOnMount={true}>
                <Navbar></Navbar>
            </Animated>
        </>
    )
}

export default App
